// bài tập 1
document.getElementById("txt-xac-nhan").addEventListener("click", function () {
  var sum = 0;
  var number = 0;
  while (sum < 10000) {
    number++;
    sum += number;
  }
  console.log(number);
  document.getElementById(
    "ket-qua"
  ).innerHTML = ` <div>Số nguyên dương nhỏ nhất là : ${number}</div>`;
});
// bài tập 2
document.getElementById("txt-ket-qua").addEventListener("click", function () {
  var soX = document.getElementById("txt-so-x").value * 1;
  var soN = document.getElementById("txt-so-n").value * 1;
  var lt = 1;
  var ketQua = 0;
  for (var i = 1; i <= soN; i++) {
    lt = soX ** i;
    ketQua += lt;
  }
  console.log(ketQua);
  document.getElementById(
    "txt-tong"
  ).innerHTML = `<div>Tổng là : ${ketQua}</div>`;
});
// bài tập 3
document.getElementById("txt-giai-thua").addEventListener("click", function () {
  var soNguyenN = document.getElementById("so-n").value * 1;
  var gt = 1;

  for (a = 1; a <= soNguyenN; a++) {
    gt = gt * a;
    console.log("giai thừa", gt);
  }
  console.log("giai thừa", gt);
  document.getElementById(
    "giai-thua"
  ).innerHTML = `<div>Giai thừa : ${gt}</div>`;
});
// bài tập 4
document.getElementById("chan-le").addEventListener("click", function () {

    var layChanLe = function(i){
        if(i%2==0){
            return `<div class="bg-danger"> Div chẵn ${i}</div>`;
        }else{
            return `<div class="bg-primary"> Div lẻ ${i}</div>`;
        }
    };
    var content="";
    for(var i=1;i<=10;i++){
        content += layChanLe(i);
    }
  document.getElementById("tao-div").innerHTML = `<div><br /> ${content}</div>`;
});
